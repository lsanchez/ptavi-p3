#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import json
import urllib.request
import smallsmilhandler
from xml.sax import make_parser


class KaraokeLocal:

    def __init__(self, file):

        parser = make_parser()
        cHandler = smallsmilhandler.SmallSMILHandler()
        parser.setContentHandler(cHandler)
        parser.parse(open(file))
        self.lista = cHandler.get_tags()

    def __str__(self):

        resultado = ''
        for dato in self.lista:
            resultado = resultado + dato[0]
            for atributo, valor in dato[1].items():
                resultado += '\t' + atributo + "=" + "'" + valor + "'"
            resultado += '\n'
        return resultado

    def to_json(self, file):

        outfile = file.replace('.smil', '.json')
        with open(outfile, 'w') as fichjson:
            json.dump(self.lista, fichjson, indent=3)

    def do_local(self):

        for dato in self.lista:
            for atributo, valor in dato[1].items():
                if atributo == 'src' and valor.startswith('http:'):
                    dir = valor.split('/')[-1]
                    print("Waiting...")
                    urllib.request.urlretrieve(valor, dir)
                    dato[1]['src'] = dato[1]['src'].replace(valor, dir)


if __name__ == "__main__":

    try:
        file = sys.argv[1]
    except IndexError:
        sys.exit("Error")

    karaoke = KaraokeLocal(file)
    print(karaoke)
    karaoke.to_json(file)
    karaoke.do_local()
    karaoke.to_json('local.json')
    print(karaoke)
